﻿using NUnit.Framework;
using System;using System.Collections.Generic;using System.Linq;using System.Text;using OpenQA.Selenium;using System.Threading.Tasks;using OpenQA.Selenium.Chrome;using OpenQA.Selenium.Interactions;using System.Threading;


namespace pom_tests
{
    public class PageDroppable
    {
        
        public PageDroppable()
        {

            IWebDriver driver;
            driver = new ChromeDriver();
            //driver.Url = "https://demoqa.com/droppable/";
         
            Actions builder = new Actions(driver);

            IWebElement smallSquare = driver.FindElement(By.Id("draggable"));
            IWebElement toDestination = driver.FindElement(By.Id("droppable"));
            builder.DragAndDrop(smallSquare, toDestination).Perform();
            private string ExpectedDroppable = "Dropped!";
            private var  result = driver.FindElement(By.Id("//p[text()='Dropped!']"));
            Assert.AreEqual(ExpectedDroppable, result);

    }
    }
}
