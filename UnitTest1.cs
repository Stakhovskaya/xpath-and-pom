using NUnit.Framework;
using System;using System.Collections.Generic;using System.Linq;using System.Text;using OpenQA.Selenium;using OpenQA.Selenium.Firefox;using System.Threading.Tasks;using OpenQA.Selenium.Chrome;using OpenQA.Selenium.Interactions;using System.Threading;

namespace XPath_tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            IWebDriver driver;
            driver = new ChromeDriver();
            driver.Url = "https://github.com/";            String PageURL = driver.Url;// Storing URL in String variable
            Console.WriteLine("URL of the page is : " + PageURL);// Printing url on console

            //-----------finding element,containing iframe, using XPath--------------
            IWebElement element1 = driver.FindElement(By.XPath("//footer/descendant ::a[text() =   'Terms' ]//ancestor:: ul//" +
"preceding-sibling::ul/descendant::a[contains(@title, 'organization')]"));            IWebElement element2 = driver.FindElement(By.XPath("//header/descendant::img [contains(@src, 'assets')]"));            IWebElement element3 = driver.FindElement(By.XPath("/descendant::a[contains (@href,'privacy')]"));            IWebElement element4 = driver.FindElement(By.XPath("//descendant::a[contains (@href,'/topics')]"));            IWebElement element5 = driver.FindElement(By.XPath("//descendant::a[contains (@href,'/join?plan=business&')]"));            IWebElement element6 = driver.FindElement(By.XPath("//descendant::a[contains (@href,'sap')]//descendant::span[text()='Read more']"));            IWebElement element7 = driver.FindElement(By.XPath("//descendant::div[contains (@aria-label,'Google')]"));            IWebElement element8 = driver.FindElement(By.XPath("//descendant::a[contains (@href, '/per')]"));            IWebElement element9 = driver.FindElement(By.XPath("/descendant::div[contains(@class, 'appli')]//descendant::button[contains(@class, 'btn')]"));            IWebElement element10 = driver.FindElement(By.XPath("//descendant::a[contains(@data-ga-click, 'Footer, go to home')]"));            IWebElement element11 = driver.FindElement(By.XPath("//descendant::input[contains(@type, 'text') and contains (@data-hotkey, 's')]"));            IWebElement element12 = driver.FindElement(By.XPath("//descendant::input[contains(@type, 'text') and contains (@data-hotkey, 's')]"));            element12.Click();            element12.SendKeys("WebDriver");            element12.SendKeys(Keys.Return);
            driver.Quit();
            Assert.Pass();
        }
    }
}